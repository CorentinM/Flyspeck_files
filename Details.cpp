#include <linbox/linbox-config.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <linbox/util/timer.h>
#include <linbox/ring/modular.h>
#include <givaro/zring.h>
#include <givaro/givrational.h>
#include <givaro/givpower.h>
#include <givaro/givquotientdomain.h>

#include <linbox/matrix/sparse-matrix.h>
#include <linbox/solutions/charpoly.h>
#include <linbox/solutions/det.h>

#include <givaro/modular-integer.h>

#include "fflas-ffpack/fflas-ffpack-config.h"
#include "fflas-ffpack/fflas-ffpack.h"
#include "fflas-ffpack/utils/fflas_io.h"


using namespace std;
using namespace LinBox;
using namespace Givaro;

typedef QField<Rational> Rationals;
typedef ZRing<Integer> IntDom;
//typedef Modular<Integer> Field;
typedef Modular<double> Field;

#include <linbox/polynomial/dense-polynomial.h>
typedef PolynomialRing<IntDom, Dense> Polynomials;
typedef PolynomialRing<Field, Dense> PolyMod;

template <class Field, class Polynomial>
std::ostream& printPolynomial (std::ostream& out, const Field &F, const Polynomial &v)
{
	for (int i = (int)(v.size () - 1); i >= 0; i--) {
		F.write (out, v[(size_t)i]);
		if (i > 0)
			out << " X^" << i << " + ";
	}
	return out;
}

void disp(DenseMatrix<Rationals> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << (double)A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void disp(DenseMatrix<IntDom> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void disp(DenseMatrix<Field> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

vector<vector<double>> read(string s, int &nb){
	ifstream fichier(s, ios::in);
	if(fichier){cout << "file successfully opened !" << endl;}
	else{cout << "error while opening file !" << endl;}
	vector<vector<double>> ret;
	
	int size;
	fichier >> nb;
	cout << nb << " matrix to read." << endl;
	vector<double> vec;
	double tmp;
	int bound;
	cout << "Reading matrix ";
	for(int j=0; j<nb; j++){
		cout << j << " ";
		vec.clear();
		fichier >> size;
		bound = size*(size+1)/2;
		for(int i=0; i<bound; i++){
			fichier >> tmp;
			vec.push_back(tmp);
		}
		ret.push_back(vec);
	}
	cout << endl;
	return ret;
}

//convert vec<double> in Rational matrix
DenseMatrix<Rationals> dToQ(vector<vector<double>> vec, int nb){
	Rationals Q;
	DenseMatrix<Rationals> A (Q);
	int size = (sqrt(1+8*vec[nb].size())-1)/2;
	A.init(Q, size, size);
	int pos;
	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
			if(j>=i){
				pos = ((size)*(size+1)/2)-((size-i)*(size-i+1)/2)+j-i;
				A.setEntry(i,j,Rational(vec[nb][pos]));
			}
			else{
				pos = ((size)*(size+1)/2)-((size-j)*(size-j+1)/2)+i-j;
				A.setEntry(i,j,Rational(vec[nb][pos]));
			}
		}
	}
	return A;
}

Integer loCoMu(vector<Integer> vec){
	Integer tmp;
	if(vec.size()>1){
		lcm(tmp, vec[0], vec[1]);
		for(unsigned int i=2; i<vec.size(); i++){
			lcm(tmp, tmp, vec[i]);
		}
	}
	else{
		tmp = vec[0];
	}
	return tmp;
}

//convert Rational matrix in Integer matrix
DenseMatrix<IntDom> qToZ(DenseMatrix<Rationals> In, Integer &mul){
	IntDom ZZ;
	DenseMatrix<IntDom> A(ZZ);
	A.init(ZZ, In.rowdim(), In.coldim());
	
	vector<Integer> vecI;
	for(unsigned int i=0; i<In.rowdim(); i++){
		for(unsigned int j=0; j<In.rowdim(); j++){
			vecI.push_back(In.getEntry(i,j).deno());
		}
	}
	mul = loCoMu(vecI);
	for(unsigned int i=0; i<In.rowdim(); i++){
		for(unsigned int j=0; j<In.rowdim(); j++){
			A.setEntry(i, j, (Integer)(mul*In.getEntry(i,j).nume()/In.getEntry(i,j).deno()));
		}
	}
	return A;
}

Integer hex2dec(string hex){
	Integer result = 0;
	for(unsigned int i=0; i<hex.size(); i++){
		if(hex[i]>=48 && hex[i]<=57){
			result += (hex[i]-48)*pow(16, hex.size()-i-1);
		}
		else if(hex[i]>=97 && hex[i]<=102){
			result += (hex[i]-87)*pow(16, hex.size()-i-1);
		}
	}
	return result;
}

Field::Element hashLambdaMod(Polynomials::Element P, DenseMatrix<Rationals> A, Integer p){
	ofstream fichier("hash.txt", ios::out | ios::trunc);
	for(unsigned int i=0; i<P.size(); i++){
		fichier << P[i] << " ";
	}
	for(unsigned int i=0; i<A.rowdim(); i++){
		for(unsigned int j=0; j<A.coldim(); j++){
			fichier << A.getEntry(i,j) << " ";
		}
	}
	fichier.close();
	
	system("sha256sum hash.txt > hashp.txt");
	
	ifstream fichier2("hashp.txt", ios::in);
	string ret;
	fichier2 >> ret;
	fichier2.close();
	
	system("rm hash.txt");
	system("rm hashp.txt");
	
	Field::Element out = hex2dec(ret.substr(0,(Integer)(logtwo(p)/4)));
	return out;
}

Integer maxNorm(DenseMatrix<IntDom> A){
	Integer norm = 1;
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			if(abs(A.getEntry(i,j))>norm){norm = abs(A.getEntry(i,j));}
		}
	}
	return norm;
}

double sizeP(DenseMatrix<IntDom> A, double eps){
	int n = A.rowdim();
	Integer B = maxNorm(A);
	double mu = logtwo(n)+2.0*logtwo(B)+0.21163175;
	double alpha = mu*n/2.0;
	double c = 2.0/eps;
	double h = c*(n-1);
	double cp = c + 1.25506*(h/log(h));
	double t = log(cp)+log(3.2605+2.0817*log(cp))+log(log(2.0)+alpha);
	return t;
}

Integer randB(Integer a, Integer b){
	return rand()%b+a;
}

Field::Element delt(Field F, DenseMatrix<Field> Am){
	Field::Element delta; F.init(delta,1);
	for(int i=0; i<(int)Am.rowdim(); i++){
			F.mulin(delta,Am.getEntry(i,i)); 
	}
	return (Am.rowdim()&1?F.negin(delta):delta);
}

Field::Element evalCA(Field F, Polynomials::Element C_Ap, Integer lambda, Integer mul, int size){
	PolyMod modPols(F); 
	PolyMod::Element C_p(C_Ap.size());
	typename Polynomials::Element::template rebind<Field>()(C_p,C_Ap);
	Field::Element res,evp,two,mulp;
	F.init(two,mul); // 2^k mod p
	F.mul(evp,two,lambda); // lambda*2^k
	modPols.eval(res,C_p,evp);
	dom_power(mulp,two,size,F); // 2^(kn)
	F.divin(res,mulp); // res / (2^(kn))
	return res;
}

bool descartesRule(Polynomials::Element C_Ap, int size){
	bool ret = false;
	for(unsigned int i=0; i<C_Ap.size(); i++){
		if(C_Ap[i]>=0 && ((int)(i-size))%2==0){
			//cout << "g_" <<i<< ">0 et i-n pair : \tOK" << endl;
		}
		else if(C_Ap[i]<=0 && ((int)(i-size))%2==-1){
			//cout << "g_" <<i<< "<0 et i-n impair : \tOK" << endl;
		}
		else if(C_Ap[i]>0 && ((int)(i-size))%2==-1){
			//cout << "g_" <<i<< ">0 et i-n impair : \tpas OK !" << endl; 
			ret=true;
		}
		else if(C_Ap[i]<0 && ((int)(i-size))%2==0){
			//cout << "g_" <<i<< "<0 et i-n pair : \tpas OK !" << endl; 
			ret=true;
		}
	}
	return ret;
}

int main(int argc, char ** argv) {
	srand(time(NULL));
	system("clear");
	Timer tim; tim.clear();tim.start();
	Rationals Rat; IntDom ZZ;
	int num = 1;
	if(argc>1){
		num = atoi(argv[1]);
	}
	
	string file = "Matrix";

	cout << "-----------------------Lecture fichier----------------------------------" << endl;
	int nb;
	vector<vector<double>> vec = read(file, nb);
	cout << endl;
	
	cout << "-----------------------Matrice A----------------------------------------" << endl;
	DenseMatrix<Rationals> A (Rat);
	A = dToQ(vec, num);
	disp(A);
	int size = A.rowdim();
	
	cout << "-----------------------Matrice A'---------------------------------------" << endl;
	DenseMatrix<IntDom> Ap(ZZ);
	Integer mul;
	Ap = qToZ(A, mul);
	cout << "mul = " << mul << endl;
	disp(Ap);
	
	cout << "-----------------------Char poly C_A'------------------------------------" << endl;
	Polynomials::Element C_Ap(ZZ);
	charpoly(C_Ap, Ap);
	cout << "C_A'(X) = ";
	printPolynomial(cout, ZZ, C_Ap) << endl << endl;
	
	cout << "-----------------------Génération p, lambda------------------------------" << endl;
	IntPrimeDom IP; Integer p;	
	double epsilon = 10e-3;
	cout << "epsilon = " << epsilon << endl;
	p = randB(pow(2, sizeP(Ap, epsilon)-1.0), pow(2, sizeP(Ap, epsilon))); IP.nextprime(p,p);
	cout << "p = " << p << " of size " << logtwo(p) << endl;
	Field::Element lambda = hashLambdaMod(C_Ap, A, p);
	cout << "lambda = " << lambda << " of size " << logtwo(lambda) << endl << endl;
	
	cout << "-----------------------Matrice A mod p-----------------------------------" << endl;
	Field F(p);
	DenseMatrix<Field> Am(A, F); //Am = A mod p
	disp(Am);
	
	cout << "-----------------------Decomposition PLUQ--------------------------------" << endl;
	for(int i=0; i<(int)Am.rowdim(); i++){
			F.subin(Am.refEntry(i,i),lambda); // Am = Am-lambda*I
	}
	size_t * P = FFLAS::fflas_new<size_t>(size);
	size_t * Q = FFLAS::fflas_new<size_t>(size);
	
	FFPACK::PLUQ(F, FFLAS::FflasNonUnit, size, size, Am.getPointer(), size, P, Q); //Am = LU
	
	FFLAS::WritePermutation (std::cout<<"P = "<<std::endl,P,size);
	FFLAS::WriteMatrix (std::cout<<"LU = "<<std::endl,F,size,size,Am.getPointer(),size)<< " modulo " << p << std::endl;
	FFLAS::WritePermutation (std::cout<<"Q = "<<std::endl,Q,size) << endl;
	
	cout << "-----------------------Verif deg(C_A) == n-------------------------------" << endl;
	cout << "degree(C_A) = " << C_Ap.size()-1 << endl;
	cout << "n = " << size << endl << endl;
	
	cout << "-----------------------Verif delta = C_A(lambda)-------------------------" << endl;
	Field::Element delta = delt(F, Am); // -delta = produit des Uii lut sur diag Am, mod p
	cout << "delta = |A mod p-I*lambda| mod p = " << delta << endl; 

	Field::Element res = evalCA(F, C_Ap, lambda, mul, size);
	cout << "C_A(lambda) = " << res << endl; 	
	
	cout << endl << "-----------------------Règle Descartes-----------------------------------" << endl;
	for(unsigned int i=0; i<C_Ap.size(); i++){
		if(C_Ap[i]>=0 && ((int)(i-size))%2==0){cout << "g_" <<i<< ">0 et i-n pair : \tOK" << endl;}
		else if(C_Ap[i]<=0 && ((int)(i-size))%2==-1){cout << "g_" <<i<< "<0 et i-n impair : \tOK" << endl;}
		else if(C_Ap[i]>0 && ((int)(i-size))%2==-1){cout << "g_" <<i<< ">0 et i-n impair : \tpas OK !" << endl;}
		else if(C_Ap[i]<0 && ((int)(i-size))%2==0){cout << "g_" <<i<< "<0 et i-n pair : \tpas OK !" << endl;}
	}
	
	tim.stop();
	cout << endl << tim << endl;

	return 0;
}

