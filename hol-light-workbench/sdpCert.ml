open Big_int;;
open Unix;;

let tm = Unix.gettimeofday();;

(*Sys.command "clear";;*)


let mul_inv a b =
	let rec aux a b x0 x1 =
		if (le_big_int a (big_int_of_string "1")) then x1 else
		if (eq_big_int b (big_int_of_string "0")) then failwith "mul_inv" else
		aux b (mod_big_int a b) (sub_big_int x1 (mult_big_int (div_big_int a b) x0)) x0
	in
	let x = aux a b (big_int_of_string "0") (big_int_of_string "1") in
	if (lt_big_int x (big_int_of_string "0")) then (add_big_int x b) else x;;


let eval_CP p coeffsTmod lambda mul size = 
	let two = mod_big_int mul p in
	let evp = mod_big_int (mult_big_int two lambda) p in
	let mulp = mod_big_int (power_big_int_positive_int two size) p in
	let inv = mul_inv mulp p in
	let eval = ref (big_int_of_string "0") in
	for i=0 to size do
		let pow = power_big_int_positive_int evp i in
		let pow = mod_big_int pow p in
		let term = mult_big_int coeffsTmod.(i) pow in
		let term = mod_big_int term p in
		eval := add_big_int !eval term
		done;
	mod_big_int (mult_big_int !eval inv) p;;


let nbT = int_of_string Sys.argv.(1);;


print_string("\nCERTIFICATE CHECKS");
for k=1 to nbT do

	let fichier = open_in "../Certificate" in

	let line = input_line fichier in
	let nb = int_of_string line in

	for i=0 to nb-1 do 
		let line = input_line fichier in
		let size = int_of_string line in
	
		print_string("\n---------------------------------------------------------------------------------" ^ "\n");
	
		if size>1 then
			let line = input_line fichier in
			let coeff = big_int_of_string line in
		
			let p_PLUQ = Array.make size 0 in
			let l_PLUQ = Array.make_matrix size size (big_int_of_string "0") in
			let u_PLUQ = Array.make_matrix size size (big_int_of_string "0") in
			let q_PLUQ = Array.make size 0 in
			let v = Array.make size (big_int_of_string "0") in
			let ap1 = Array.make size (big_int_of_string "0") in
			let ap2 = Array.make size (big_int_of_string "0") in
			let ap3 = Array.make size (big_int_of_string "0") in
			let coeffsT = Array.make (size+1) coeff in
			let coeffsTmod = Array.make (size+1) coeff in
		
			for j=1 to size do
				let line = input_line fichier in
				let coeff = big_int_of_string line in
				Array.set coeffsT j coeff;
				done;
		
			let line = input_line fichier in
			let delta = big_int_of_string line in
		
			let line = input_line fichier in
			let lambda = big_int_of_string line in
			(*
			print_string ("Lambda = " ^ line ^ "\n");
			*)
		
			let line = input_line fichier in
			let mul = big_int_of_string line in
			(*
			print_string ("mul = " ^ line ^ "\n");
			*)
		
			let line = input_line fichier in
			let p = big_int_of_string line in
			(*
			print_string ("p = " ^ line ^ "\n");
			*)
	
			let matAp = Array.make_matrix size size (big_int_of_string "0") in
			for i=0 to size-1 do
				for j=0 to size-1 do
					if(j>=i) then
						let line = input_line fichier in
						let slash = (String.contains line '/') in
						if(slash) then
							let ind = String.index line '/' in
							let nume = big_int_of_string (String.sub line 0 ind) in
							let deno = big_int_of_string (String.sub line (ind+1) ((String.length line)-ind-1)) in
							matAp.(i).(j) <- mod_big_int (mult_big_int nume (mul_inv deno p)) p;
							matAp.(j).(i) <- matAp.(i).(j)
						else 
							matAp.(i).(j) <- mod_big_int (big_int_of_string line) p;
							matAp.(j).(i) <- matAp.(i).(j)
					done;
				done;
		
			(*
			for i=0 to size-1 do
				for j=0 to size-1 do
					print_string(string_of_big_int matAp.(i).(j) ^ "\t");
					done;
				print_string("\n");
				done;
			*)
		
			for i=0 to size-1 do
				matAp.(i).(i) <- mod_big_int (sub_big_int matAp.(i).(i) lambda) p;
				done;
			
			(*
			for i=0 to size-1 do
				for j=0 to size-1 do
					print_string(string_of_big_int matAp.(i).(j) ^ "\t");
					done;
				print_string("\n");
				done;
			*)
		
			for i=0 to size-1 do
				let line = input_line fichier in
				p_PLUQ.(i) <- int_of_string line;
				done;
			
			for i=0 to size-1 do
				for j=0 to size-1 do
					let line = input_line fichier in
					if(j>i) then begin
						u_PLUQ.(i).(j) <- big_int_of_string line end
					else if(j<i) then begin
						l_PLUQ.(i).(j) <- big_int_of_string line end
					else if(j=i) then begin
						u_PLUQ.(i).(j) <- big_int_of_string line;
						l_PLUQ.(i).(j) <- big_int_of_string "1" end
					done;
				done;
			
			for i=0 to size-1 do
				let line = input_line fichier in
				q_PLUQ.(i) <- int_of_string line;
				done;
			
			(*
			for i=0 to size-1 do
				for j=0 to size-1 do
					let pr = string_of_big_int u_PLUQ.(i).(j) in
					print_string(pr ^ "\t");
					done;
				print_string("\n");
				done;
			print_string("\n");
			
			for i=0 to size-1 do
				for j=0 to size-1 do
					let pr = string_of_big_int l_PLUQ.(i).(j) in
					print_string(pr ^ "\t");
					done;
				print_string("\n");
				done;
			*)
		
		
		
		
			Random.self_init();
			for i=0 to size-1 do
				let rand64 = Random.int64 (int64_of_big_int (sub_big_int p (big_int_of_string "2"))) in
				let rand = big_int_of_int64 rand64 in
				v.(i) <- add_big_int rand (big_int_of_string "1");
				done;
		
			(* Apply q *)
			for i=0 to size-1 do
				if (q_PLUQ.(i)!=i) then begin
					let tmp = v.(q_PLUQ.(i)) in
					v.(q_PLUQ.(i)) <- v.(i);
					v.(i) <- tmp; end
				done;
			(*
			for i=0 to size-1 do
				let pr = string_of_big_int v.(i) in
				print_string(pr ^ "\t");
				done;
			print_string("\n");
			*)
		
			(* Apply U *)
			for i=0 to size-1 do
				for j=0 to size-1 do
					ap1.(i) <- mod_big_int (add_big_int ap1.(i) (mult_big_int v.(j) u_PLUQ.(i).(j))) p;
					done;
				done;
			(*
			for i=0 to size-1 do
				let pr = string_of_big_int ap1.(i) in
				print_string(pr ^ "\t");
				done;
			print_string("\n");
			*)

			(* Apply L *)
			for i=0 to size-1 do
				for j=0 to size-1 do
					ap2.(i) <- mod_big_int (add_big_int ap2.(i) (mult_big_int ap1.(j) l_PLUQ.(i).(j))) p;
					done;
				done;
			(*
			for i=0 to size-1 do
				let pr = string_of_big_int ap2.(i) in
				print_string(pr ^ "\t");
				done;
			print_string("\n");
			*)
		
			(* Apply P *)
			for i=0 to size-1 do
				if (p_PLUQ.(i)!=i) then begin
					let tmp = ap2.(p_PLUQ.(i)) in
					ap2.(p_PLUQ.(i)) <- ap2.(i);
					ap2.(i) <- tmp; end
				done;
				
			(* Apply v on matAp=Ap-lambda*I *)
			for i=0 to size-1 do
				for j=0 to size-1 do
					ap3.(i) <- mod_big_int (add_big_int ap3.(i) (mult_big_int v.(j) matAp.(i).(j))) p;
					done;
				done;
		
			
			print_string("P(L(U(Qv))) mod p = \n[");
			for i=0 to size-2 do
				let pr = string_of_big_int ap2.(i) in
				print_string(pr ^ ", ");
				done;
			let pr = string_of_big_int ap2.(size-1) in
			print_string(pr);
			print_string("]\n");
		
			print_string("(Ap - lambda*I)v mod p = \n[");
			for i=0 to size-2 do
				let pr = string_of_big_int ap3.(i) in
				print_string(pr ^ ", ");
				done;
			let pr = string_of_big_int ap3.(size-1) in
			print_string(pr);
			print_string("]\n\n");
			




			for j=0 to size do
				(Array.set coeffsTmod j (mod_big_int coeffsT.(j) p));
				done;
		
			let pr = string_of_big_int delta in
			print_string ("Delta =		\t" ^ pr ^ "\n");
			let ev = string_of_big_int(eval_CP p coeffsTmod lambda mul size) in
			print_string("C_A(lambda) mod p = \t" ^ ev ^ "\n\n");
		
			
			let zero = zero_big_int in
			for j=0 to size do
				if((j-size) mod 2 = 0 && (ge_big_int (Array.get coeffsT j) zero)) then begin
					print_string ("OK ") end
				else if ((j-size) mod 2 = -1 && (le_big_int (Array.get coeffsT j) zero)) then begin
					print_string ("OK ") end
				else begin
					print_string ("Eigenvalues problem !\n") end
				done;
			
		else
			let line = input_line fichier in
			if((String.get line 0)!='-') then
				print_string("OK ")
			else
				print_string ("Eigenvalues problem !\n")
		done;
	close_in fichier;
	done;;
	
print_string("\n---------------------------------------------------------------------------------" ^ "\n");

Printf.printf "Average execution time : %fs\n" ((Unix.gettimeofday()-. tm)/.(float_of_int nbT));
print_string("\n");

