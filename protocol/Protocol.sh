#!/bin/bash

mv ineq.m ../matlab
cd ../matlab
matlab -r "run('ineq.m'), quit"
cd ..
make charPolyCert
./charPolyCert
. hol-light-workbench/setpaths
ocamlc nums.cma unix.cma hol-light-workbench/sdpCert.ml -o hol-light-workbench/sdpCert.exe
./hol-light-workbench/sdpCert.exe 1
