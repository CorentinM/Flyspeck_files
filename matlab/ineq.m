clear all
close all

r = 1; %Degree of monomial vector v0, degree+1 of other monomial vectors
bound0 = 0; %Lower bound on mat0
ops = sdpsettings('solver','sedumi','sedumi.eps',1e-12, 'cachesolvers', 1);
prec = 6; %Precision on approximation for the matrices


x1 = sdpvar(1,1);
x2 = sdpvar(1,1);
x3 = sdpvar(1,1);
x4 = sdpvar(1,1);
x5 = sdpvar(1,1);
x6 = sdpvar(1,1);
vec = [x1 x2 x3 x4 x5 x6];

%Polynomial and intervals to fill-----------------
	p = x1*x4*(-x1+x2+x3-x4+x5+x6)+x2*x5*(x1-x2+x3+x4-x5+x6)+x3*x6*(x1+x2-x3+x4+x5-x6)-x2*x3*x4-x1*x3*x5-x1*x2*x6-x4*x5*x6;
    Cx1 = [4 6.3504];
    Cx2 = [4 6.3504]; 
    Cx3 = [4 6.3504]; 
    Cx4 = [4 6.3504];
    Cx5 = [4 6.3504]; 
    Cx6 = [4 6.3504];
%-------------------------------------------------

x1 = (x1-Cx1(1))/(Cx1(2)-Cx1(1));
C1 = (x1-0)*(-x1+1);

x2 = (x2-Cx2(1))/(Cx2(2)-Cx2(1));
C2 = (x2-0)*(-x2+1);

x3 = (x3-Cx3(1))/(Cx3(2)-Cx3(1));
C3 = (x3-0)*(-x3+1);

x4 = (x4-Cx4(1))/(Cx4(2)-Cx4(1));
C4 = (x4-0)*(-x4+1);

x5 = (x5-Cx5(1))/(Cx5(2)-Cx5(1));
C5 = (x5-0)*(-x5+1);

x6 = (x6-Cx6(1))/(Cx6(2)-Cx6(1));
C6 = (x6-0)*(-x6+1);

v0 = monolist(vec,r);
v = monolist(vec,r-1);

Q0 = sdpvar(length(v0));
Qx1 = sdpvar(length(v));
Qx2 = sdpvar(length(v));
Qx3 = sdpvar(length(v));
Qx4 = sdpvar(length(v));
Qx5 = sdpvar(length(v));
Qx6 = sdpvar(length(v));

s0 = v0'*Q0*v0;
s1 = v'*Qx1*v;
s2 = v'*Qx2*v;
s3 = v'*Qx3*v;
s4 = v'*Qx4*v;
s5 = v'*Qx5*v;
s6 = v'*Qx6*v;

p_sos = s0 + s1*C1 + s2*C2 + s3*C3 + s4*C4 + s5*C5 + s6*C6;
F = [coefficients(p-p_sos,vec) == 0, Q0 >= bound0, Qx1 >= 0, Qx2 >= 0, Qx3 >= 0, Qx4 >= 0, Qx5 >= 0, Qx6 >= 0];
diagnostics = optimize(F,[], ops);

mat0 = roundn(value(Q0),-prec);
matx1 = roundn(value(Qx1),-prec);
matx2 = roundn(value(Qx2),-prec);
matx3 = roundn(value(Qx3),-prec);
matx4 = roundn(value(Qx4),-prec);
matx5 = roundn(value(Qx5),-prec);
matx6 = roundn(value(Qx6),-prec);

size_mat0 = length(mat0)
size_matx1 = length(matx1)


res = v0'*mat0*v0 + v'*matx1*v*C1 + v'*matx2*v*C2 + v'*matx3*v*C3 + v'*matx4*v*C4 + v'*matx5*v*C5 + v'*matx6*v*C6;
assign(vec,randn(1,length(vec)));
value(p)
value(res)


file = fopen('../Matrix', 'w');
fprintf(file,'%i\n',7);

fprintf(file,'%i ',length(mat0));
for i = 1:length(mat0)
	for j = 1:length(mat0)
		if(j>=i)
			fprintf(file,'%i ',mat0(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx1));
for i = 1:length(matx1)
	for j = 1:length(matx1)
		if(j>=i)
			fprintf(file,'%i ',matx1(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx2));
for i = 1:length(matx2)
	for j = 1:length(matx2)
		if(j>=i)
			fprintf(file,'%i ',matx2(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx3));
for i = 1:length(matx3)
	for j = 1:length(matx3)
		if(j>=i)
			fprintf(file,'%i ',matx3(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx4));
for i = 1:length(matx4)
	for j = 1:length(matx4)
		if(j>=i)
			fprintf(file,'%i ',matx4(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx5));
for i = 1:length(matx5)
	for j = 1:length(matx5)
		if(j>=i)
			fprintf(file,'%i ',matx5(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx6));
for i = 1:length(matx6)
	for j = 1:length(matx6)
		if(j>=i)
			fprintf(file,'%i ',matx6(i,j));
		end
	end
end
fprintf(file,'\n');

fclose(file);
