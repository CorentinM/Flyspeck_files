clear all
close all

nb = 1; %Size of the sample for average
r = 1; %Degree of monomial vector v0, degree+1 of other monomial vectors
bound0 = 0; %Lower bound on mat0
ops = sdpsettings('solver','sedumi','sedumi.eps',1e-12, 'cachesolvers', 1);
prec = 6; %Precision on approximation for the matrices

time = 0;
for i=1:nb
	tic;

	x1 = sdpvar(1,1);
	x2 = sdpvar(1,1);
	x3 = sdpvar(1,1);
	vec = [x1 x2 x3];

	%Polynomial and intervals to fill-----------------
	p = 9.0601*x1-x1*(-x1+x2+x3+8-9.0601);
    Cx1 = [4 6.3504];
    Cx2 = [4 6.3504]; 
    Cx3 = [4 6.3504];
	%-------------------------------------------------

	x1 = (x1-Cx1(1))/(Cx1(2)-Cx1(1));
	C1 = (x1-0)*(-x1+1);

	x2 = (x2-Cx2(1))/(Cx2(2)-Cx2(1));
	C2 = (x2-0)*(-x2+1);

	x3 = (x3-Cx3(1))/(Cx3(2)-Cx3(1));
	C3 = (x3-0)*(-x3+1);

	v0 = monolist(vec,r);
	v = monolist(vec,r-1);

	Q0 = sdpvar(length(v0));
	Qx1 = sdpvar(length(v));
	Qx2 = sdpvar(length(v));
	Qx3 = sdpvar(length(v));

	s0 = v0'*Q0*v0;
	s1 = v'*Qx1*v;
	s2 = v'*Qx2*v;
	s3 = v'*Qx3*v;

	p_sos = s0 + s1*C1 + s2*C2 + s3*C3;
	F = [coefficients(p-p_sos,vec) == 0, Q0 >= bound0, Qx1 >= 0, Qx2 >= 0, Qx3 >= 0];
	diagnostics = optimize(F,[], ops);
	toc;
	if(i>5)
		time = time + toc;
	end
end
time = time/nb

mat0 = roundn(value(Q0),-prec);
matx1 = roundn(value(Qx1),-prec);
matx2 = roundn(value(Qx2),-prec);
matx3 = roundn(value(Qx3),-prec);

size_mat0 = length(mat0)
size_matx1 = length(matx1)


res = v0'*mat0*v0 + v'*matx1*v*C1 + v'*matx2*v*C2 + v'*matx3*v*C3;
assign(vec,randn(1,length(vec)));
value(p)
value(res)


file = fopen('../Matrix', 'w');
fprintf(file,'%i\n',4);

fprintf(file,'%i ',length(mat0));
for i = 1:length(mat0)
	for j = 1:length(mat0)
		if(j>=i)
			fprintf(file,'%i ',mat0(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx1));
for i = 1:length(matx1)
	for j = 1:length(matx1)
		if(j>=i)
			fprintf(file,'%i ',matx1(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx2));
for i = 1:length(matx2)
	for j = 1:length(matx2)
		if(j>=i)
			fprintf(file,'%i ',matx2(i,j));
		end
	end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx3));
for i = 1:length(matx3)
	for j = 1:length(matx3)
		if(j>=i)
			fprintf(file,'%i ',matx3(i,j));
		end
	end
end
fprintf(file,'\n');

fclose(file);
