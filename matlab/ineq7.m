clear all
close all

nb = 1;

% r = 1;
% bound0 = 0;

r = 1;
bound0 = 0.000001;

time = 0;
for i=1:nb
    tic;
    
    x1 = sdpvar(1,1);
    vec = [x1];

    tmp = -x1+9.04*sqrt(x1)-0.2704;
    tmp2 = 28.8608;
    
    p = (pi/3)+atan((2.3504-x1)/(sqrt(tmp)))+atan((-2.3504-x1)/(sqrt(tmp)))+atan((-8.7008)/(sqrt(tmp2)));
    Cx1 = [4 6.3504];

    x1 = (x1-Cx1(1))/(Cx1(2)-Cx1(1));
    C1 = (x1-0)*(-x1+1); %Constraint on x1

    v0 = monolist(vec,r);
    v = monolist(vec,r-1);

    Q0 = sdpvar(length(v0));
    Qx1 = sdpvar(length(v));

    s1 = v'*Qx1*v;
    s0 = v0'*Q0*v0;

    p_sos = s0 + s1*C1;
    ops = sdpsettings('solver','sedumi','sedumi.eps',1e-12, 'cachesolvers', 1);
    F = [coefficients(p-p_sos,vec) == 0, Qx1 >= 0, Q0 >= bound0];
    diagnostics = optimize(F,[], ops);
    toc;
    if(i>5)
        time = time + toc;
    end
end
time = time/nb

prec = 6;
matx1 = roundn(value(Qx1),-prec);
mat0 = roundn(value(Q0),-prec);

% sdisplay(p)
% sdisplay(res)
% lambda = value(lambda)

% assign([x1 x2 x3 x4 x5 x6],randn(1,6));
% sdisplay(clean(value(p-res), 1e-6))

size_mat0 = length(mat0)
size_matx1 = length(matx1)

file = fopen('../Matrix', 'w');
fprintf(file,'%i\n',2);

fprintf(file,'%i ',length(mat0));
for i = 1:length(mat0)
    for j = 1:length(mat0)
        if(j>=i)
            fprintf(file,'%i ',mat0(i,j));
        end
    end
end
fprintf(file,'\n');

fprintf(file,'%i ',length(matx1));
for i = 1:length(matx1)
    for j = 1:length(matx1)
        if(j>=i)
            fprintf(file,'%i ',matx1(i,j));
        end
    end
end
fprintf(file,'\n');

fclose(file);
