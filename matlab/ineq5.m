clear all
close all

nb = 1; %Size of the sample for average
r = 1; %Degree of monomial vector v0, degree+1 of other monomial vectors
bound0 = 0; %Lower bound on mat0
ops = sdpsettings('solver','sedumi','sedumi.eps',1e-12, 'cachesolvers', 1);
prec = 6; %Precision on approximation for the matrices

time = 0;
for i=1:nb
	tic;

	vec = [];

	%Polynomial and intervals to fill-----------------
	p = -0.591 + 0.331*64 - ((0.506*1.26)/(0.26)) + ((0.506)/(0.26)) - 1;
	%-------------------------------------------------

	v0 = monolist(vec,r);

	Q0 = sdpvar(length(v0));

	s0 = v0'*Q0*v0;

	p_sos = s0;
	F = [coefficients(p-p_sos,vec) == 0, Q0 >= bound0];
	diagnostics = optimize(F,[], ops);
	toc;
	if(i>5)
		time = time + toc;
	end
end
time = time/nb

mat0 = roundn(value(Q0),-prec);

size_mat0 = length(mat0)


res = v0'*mat0*v0;
value(p)
value(res)


file = fopen('../Matrix', 'w');
fprintf(file,'%i\n',1);

fprintf(file,'%i ',length(mat0));
for i = 1:length(mat0)
	for j = 1:length(mat0)
		if(j>=i)
			fprintf(file,'%i ',mat0(i,j));
		end
	end
end
fprintf(file,'\n');

fclose(file);
