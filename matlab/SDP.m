clear all
close all

tic;

x = sdpvar(1,1);
y = sdpvar(1,1);
vec = [x y];
p = x^4*y^2+x^2*y^4-3*x^2*y^2+1;
%p = (x^2+y^2)^2;
%p = (1+x)^4 + (1-y)^2;

[s2,c] = polynomial(vec,2);
[sol, v, W] = solvesos([sos(s2),sos(p*s2)],[],[],c);

t = toc;



mat1 = W{2}
v1 = v{2}'; sdisplay(v1)
eig1 = eig(mat1)'

mat2 = W{1}
v2 = v{1}'; sdisplay(v2)
eig2 = eig(mat2)'



assign(vec,randn(1,length(vec)));
value(p)
value(v{2}'*mat1*v{2})/value(v{1}'*mat2*v{1})

t