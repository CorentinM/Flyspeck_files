#!/bin/bash

file=ineq.m

rm $file
echo "clear all" >> $file
echo "close all" >> $file
echo "" >> $file
echo "nb = 1; %Size of the sample for average" >> $file
echo "r = 1; %Degree of monomial vector v0, degree+1 of other monomial vectors" >> $file
echo "bound0 = 0; %Lower bound on mat0" >> $file
echo "ops = sdpsettings('solver','sedumi','sedumi.eps',1e-12, 'cachesolvers', 1);" >> $file
echo "prec = 6; %Precision on approximation for the matrices" >> $file
echo "" >> $file
echo "time = 0;" >> $file
echo "for i=1:nb" >> $file
echo "	tic;" >> $file
echo "" >> $file
for i in `seq 1 $1`; do
	echo "	x"$i" = sdpvar(1,1);" >> $file
done

echo -n "	vec = [" >> $file
if [ $1 -ge 1 ] 
then
	echo -n "x1" >> $file
fi
for i in `seq 2 $1`; do
	echo -n " x"$i >> $file
done
echo "];" >> $file
echo "" >> $file
echo "	%Polynomial and intervals to fill-----------------" >> $file
echo "	p = 1;" >> $file
for i in `seq 1 $1`; do
	echo "	Cx"$i" = [0 1];" >> $file
done
echo "	%-------------------------------------------------" >> $file
echo "" >> $file
for i in `seq 1 $1`; do
	echo "	x"$i" = (x"$i"-Cx"$i"(1))/(Cx"$i"(2)-Cx"$i"(1));" >> $file
    echo "	C"$i" = (x"$i"-0)*(-x"$i"+1);" >> $file
    echo "" >> $file
done
echo "	v0 = monolist(vec,r);" >> $file
if [ $1 -ge 1 ] 
then
	echo "	v = monolist(vec,r-1);" >> $file
fi
echo "" >> $file
echo "	Q0 = sdpvar(length(v0));" >> $file
for i in `seq 1 $1`; do
	echo "	Qx"$i" = sdpvar(length(v));" >> $file
done
echo "" >> $file
echo "	s0 = v0'*Q0*v0;" >> $file
for i in `seq 1 $1`; do
	echo "	s"$i" = v'*Qx"$i"*v;" >> $file
done
echo "" >> $file
echo -n "	p_sos = s0" >> $file
for i in `seq 1 $1`; do
	echo -n " + s"$i"*C"$i >> $file
done
echo ";" >> $file
echo -n "	F = [coefficients(p-p_sos,vec) == 0, Q0 >= bound0" >> $file
for i in `seq 1 $1`; do
	echo -n ", Qx"$i" >= 0" >> $file
done
echo "];" >> $file
echo "	diagnostics = optimize(F,[], ops);" >> $file
echo "	toc;" >> $file
echo "	if(i>5)" >> $file
echo "		time = time + toc;" >> $file
echo "	end" >> $file
echo "end" >> $file
echo "time = time/nb" >> $file
echo "" >> $file
echo "mat0 = roundn(value(Q0),-prec);" >> $file
for i in `seq 1 $1`; do
	echo "matx"$i" = roundn(value(Qx"$i"),-prec);" >> $file
done
echo "" >> $file
echo "size_mat0 = length(mat0)" >> $file
if [ $1 -ge 1 ] 
then
	echo "size_matx1 = length(matx1)" >> $file
fi
echo "" >> $file
echo "" >> $file

echo -n "res = v0'*mat0*v0" >> $file
for i in `seq 1 $1`; do
	echo -n " + v'*matx"$i"*v*C"$i >> $file
done
echo ";" >> $file
if [ $1 -ge 1 ] 
then
	echo "assign(vec,randn(1,length(vec)));" >> $file
fi
echo "value(p)" >> $file
echo "value(res)" >> $file
echo "" >> $file
echo "" >> $file

echo "file = fopen('../Matrix', 'w');" >> $file
tmp=$1
((tmp++))
echo "fprintf(file,'%i\n',"$tmp");" >> $file
echo "" >> $file
echo "fprintf(file,'%i ',length(mat0));" >> $file
echo "for i = 1:length(mat0)" >> $file
echo "	for j = 1:length(mat0)" >> $file
echo "		if(j>=i)" >> $file
echo "			fprintf(file,'%i ',mat0(i,j));" >> $file
echo "		end" >> $file
echo "	end" >> $file
echo "end" >> $file
echo "fprintf(file,'\n');" >> $file
echo "" >> $file

for i in `seq 1 $1`; do
	echo "fprintf(file,'%i ',length(matx"$i"));" >> $file
	echo "for i = 1:length(matx"$i")" >> $file
	echo "	for j = 1:length(matx"$i")" >> $file
	echo "		if(j>=i)" >> $file
	echo "			fprintf(file,'%i ',matx"$i"(i,j));" >> $file
	echo "		end" >> $file
	echo "	end" >> $file
	echo "end" >> $file
	echo "fprintf(file,'\n');" >> $file
	echo "" >> $file
done
echo -n "fclose(file);" >> $file
