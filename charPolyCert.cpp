#include <linbox/linbox-config.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <linbox/util/timer.h>
#include <linbox/ring/modular.h>
#include <givaro/zring.h>
#include <givaro/givrational.h>
#include <givaro/givpower.h>
#include <givaro/givquotientdomain.h>

#include <linbox/matrix/sparse-matrix.h>
#include <linbox/solutions/charpoly.h>
#include <linbox/solutions/det.h>
// #include <givaro/givpoly1.h>
#include <linbox/polynomial/dense-polynomial.h>

#include <givaro/modular-integer.h>

#include "fflas-ffpack/fflas-ffpack-config.h"
#include "fflas-ffpack/fflas-ffpack.h"
#include "fflas-ffpack/utils/fflas_io.h"

using namespace std;
using namespace LinBox;
using namespace Givaro;

typedef QField<Rational> Rationals;
typedef ZRing<Integer> IntDom;
//typedef Modular<Integer> Field;
typedef Modular<double> Field;

// typedef Poly1Dom<IntDom, Dense> Polynomials;
// typedef Poly1Dom<Field, Dense> PolyMod;
typedef PolynomialRing<IntDom, Dense> Polynomials;
typedef PolynomialRing<Field, Dense> PolyMod;


template <class Field, class Polynomial>
std::ostream& printPolynomial (std::ostream& out, const Field &F, const Polynomial &v)
{
	for (int i = (int)(v.size () - 1); i >= 0; i--) {
		F.write (out, v[(size_t)i]);
		if (i > 0)
			out << " X^" << i << " + ";
	}
	return out;
}

void disp(DenseMatrix<Rationals> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void disp(DenseMatrix<IntDom> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void disp(DenseMatrix<Field> A){
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			cout << A.getEntry(i,j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

vector<vector<double>> read(string s, int &nb){
	ifstream fichier(s, ios::in);
	if(!fichier){cout << "error while opening file !" << endl;}
	vector<vector<double>> ret;
	int size;
	fichier >> nb;
	vector<double> vec;
	double tmp;
	int bound;
	for(int j=0; j<nb; j++){
		vec.clear();
		fichier >> size;
		bound = size*(size+1)/2;
		for(int i=0; i<bound; i++){
			fichier >> tmp;
			vec.push_back(tmp);
		}
		ret.push_back(vec);
	}
	return ret;
}

//convert vec<double> in Rational matrix
DenseMatrix<Rationals> dToQ(vector<vector<double>> vec, int nb){
	Rationals Q;
	DenseMatrix<Rationals> A (Q);
	int size = (sqrt(1+8*vec[nb].size())-1)/2;
	A.init(Q, size, size);
	int pos;
	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++){
			if(j>=i){
				pos = ((size)*(size+1)/2)-((size-i)*(size-i+1)/2)+j-i;
				A.setEntry(i,j,Rational(vec[nb][pos]));
			}
			else{
				pos = ((size)*(size+1)/2)-((size-j)*(size-j+1)/2)+i-j;
				A.setEntry(i,j,Rational(vec[nb][pos]));
			}
		}
	}
	return A;
}

Integer loCoMu(vector<Integer> vec){
	Integer tmp;
	if(vec.size()>1){
		lcm(tmp, vec[0], vec[1]);
		for(unsigned int i=2; i<vec.size(); i++){
			lcm(tmp, tmp, vec[i]);
		}
	}
	else{
		tmp = vec[0];
	}
	return tmp;
}

//convert Rational matrix in Integer matrix
DenseMatrix<IntDom> qToZ(DenseMatrix<Rationals> In, Integer &mul){
	IntDom ZZ;
	DenseMatrix<IntDom> A(ZZ);
	A.init(ZZ, In.rowdim(), In.coldim());
	
	vector<Integer> vecI;
	for(unsigned int i=0; i<In.rowdim(); i++){
		for(unsigned int j=0; j<In.rowdim(); j++){
			vecI.push_back(In.getEntry(i,j).deno());
		}
	}
	mul = loCoMu(vecI);
	for(unsigned int i=0; i<In.rowdim(); i++){
		for(unsigned int j=0; j<In.rowdim(); j++){
			A.setEntry(i, j, (Integer)(mul*In.getEntry(i,j).nume()/In.getEntry(i,j).deno()));
		}
	}
	return A;
}

Integer hex2dec(string hex){
	Integer result = 0;
	for(unsigned int i=0; i<hex.size(); i++){
		if(hex[i]>=48 && hex[i]<=57){
			result += (hex[i]-48)*pow(16, hex.size()-i-1);
		}
		else if(hex[i]>=97 && hex[i]<=102){
			result += (hex[i]-87)*pow(16, hex.size()-i-1);
		}
	}
	return result;
}

Field::Element hashLambdaMod(Polynomials::Element P, DenseMatrix<IntDom> A, Integer p){
	ofstream fichier("hash.txt", ios::out | ios::trunc);
	for(unsigned int i=0; i<P.size(); i++){
		fichier << P[i];
	}
	for(unsigned int i=0; i<A.rowdim(); i++){
		for(unsigned int j=0; j<A.coldim(); j++){
			fichier << A.getEntry(i,j);
		}
	}
	fichier.close();
	
	system("sha256sum hash.txt > hashp.txt");
	
	ifstream fichier2("hashp.txt", ios::in);
	string ret;
	fichier2 >> ret;
	fichier2.close();
	
	system("rm hash.txt");
	system("rm hashp.txt");
	
	Field::Element out = hex2dec(ret.substr(0,(Integer)(logtwo(p)/4)));
	return out;
}

Integer maxNorm(DenseMatrix<IntDom> A){
	Integer norm = 1;
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			if(abs(A.getEntry(i,j))>norm){norm = abs(A.getEntry(i,j));}
		}
	}
	return norm;
}

double sizeP(DenseMatrix<IntDom> A, double eps){
	int n = A.rowdim();
	Integer B = maxNorm(A);
	double mu = logtwo(n)+2.0*logtwo(B)+0.21163175;
	double alpha = mu*n/2.0;
	double c = 2.0/eps;
	double h = c*(n-1);
	double cp = c + 1.25506*(h/log(h));
	double t = log(cp)+log(3.2605+2.0817*log(cp))+log(log(2.0)+alpha);
	return t;
}

Integer randB(Integer a, Integer b){
	return rand()%b+a;
}

Field::Element delt(Field F, DenseMatrix<Field> Am){
	Field::Element delta; F.init(delta,1);
	for(int i=0; i<(int)Am.rowdim(); i++){
			F.mulin(delta,Am.getEntry(i,i)); 
	}
	return (Am.rowdim()&1?F.negin(delta):delta);
}

Field::Element evalCA(Field F, const Polynomials::Element& C_Ap, Integer lambda, Integer mul, int size){
	PolyMod modPols(F); 
	PolyMod::Element C_p(C_Ap,F);

	Field::Element res,evp,two,mulp;
	F.init(two,mul); // 2^k mod p
	F.mul(evp,two,lambda); // lambda*2^k
	modPols.eval(res,C_p,evp);
	dom_power(mulp,two,size,F); // 2^(kn)
	F.divin(res,mulp); // res / (2^(kn))
	return res;
}

bool descartesRule(Polynomials::Element C_Ap, int size){
	bool ret = false;
	for(unsigned int i=0; i<C_Ap.size(); i++){
		if(C_Ap[i]>=0 && ((int)(i-size))%2==0){
			//cout << "g_" <<i<< ">0 et i-n pair : \tOK" << endl;
		}
		else if(C_Ap[i]<=0 && ((int)(i-size))%2==-1){
			//cout << "g_" <<i<< "<0 et i-n impair : \tOK" << endl;
		}
		else if(C_Ap[i]>0 && ((int)(i-size))%2==-1){
			//cout << "g_" <<i<< ">0 et i-n impair : \tpas OK !" << endl; 
			ret=true;
		}
		else if(C_Ap[i]<0 && ((int)(i-size))%2==0){
			//cout << "g_" <<i<< "<0 et i-n pair : \tpas OK !" << endl; 
			ret=true;
		}
	}
	return ret;
}

void writeCertificate(int size, Polynomials::Element C_Ap, Field::Element delta, Field::Element lambda, Integer mul, Integer p, DenseMatrix<Rationals> A, size_t * P, DenseMatrix<Field> Am, size_t * Q){
	ofstream fichier("Certificate", ios::out | ios::app);
	fichier << size << endl; 	//Matrix dimension
	for(unsigned int i=0; i<C_Ap.size(); i++){
		fichier << C_Ap[i] << endl; //Characteristic polynomial
	}
	fichier << delta << endl; //A-lambda*I determinant
	fichier << lambda << endl; //hash(A, C_A)
	fichier << mul << endl; //lcm of denominators of A
	fichier << p << endl; //Prime number
	for(int i=0; i<(int)A.rowdim(); i++){
		for(int j=0; j<(int)A.coldim(); j++){
			if(j>=i){fichier << A.getEntry(i,j) << endl;} //Rational matrix
		}
	}
	for(int i=0; i<size; i++){
		fichier << P[i] << endl; //Permutation matrix P 
	}
	for(int i=0; i<(int)Am.rowdim(); i++){
		for(int j=0; j<(int)Am.coldim(); j++){
			fichier << Am.getEntry(i,j) << endl; //LU in place mod p
		}
	}
	for(int i=0; i<size; i++){
		fichier << Q[i] << endl; //Permutation matrix Q
	}
	fichier.close();
}

int main(int argc, char ** argv) {
	srand(time(NULL));
	//system("clear");
	Timer tim; tim.clear();tim.start();
	
	string file = "Matrix";		//file to read
	double epsilon = 10e-3;		//epsilon value for p

	for(int i=0; i<100; i++){
		cout << endl;
	}
	cout << "CERTIFICATE GENERATION" << endl;
	int nb;
	Timer rfT; rfT.clear(); rfT.start();
	vector<vector<double>> vec = read(file, nb);
	rfT.stop();
	
	Timer vDT; vDT.clear(); vDT.start();
	Rationals Rat; IntDom ZZ;
	DenseMatrix<Rationals> A (Rat);
	int size;
	DenseMatrix<IntDom> Ap(ZZ);
	Integer mul;
	Polynomials::Element C_Ap(ZZ);
	IntPrimeDom IP; Integer p;
	Field::Element delta = 1, lambda = 1;
	size_t * P;
	size_t * Q;
	vDT.stop();
	
	Timer dTQ; dTQ.clear(); Timer dTQ2; dTQ2.clear(); 
	Timer qTZ; qTZ.clear(); Timer qTZ2; qTZ2.clear(); 
	Timer tCp; tCp.clear(); Timer tCp2; tCp2.clear();
	Timer pGen; pGen.clear(); Timer pGen2; pGen2.clear();
	Timer lamGen; lamGen.clear(); Timer lamGen2; lamGen2.clear(); 
	Timer ZTZpZ; ZTZpZ.clear(); Timer ZTZpZ2; ZTZpZ2.clear(); 
	Timer deltGen; deltGen.clear(); Timer deltGen2; deltGen2.clear();
	Timer PLUQT; PLUQT.clear(); Timer PLUQT2; PLUQT2.clear();
	
	cout << "-----------------------Generate certificates-----------------------------------" << endl;
	ofstream fichier("Certificate", ios::out | ios::trunc);
	fichier << nb << endl;
	fichier.close();
	for(int i=0; i<nb; i++){
		dTQ.start(); A = dToQ(vec, i); dTQ.stop(); dTQ2 += dTQ;
		size = A.rowdim();
		if(size>1){
			qTZ.start(); Ap = qToZ(A, mul); qTZ.stop(); qTZ2 += qTZ;
			tCp.start(); charpoly(C_Ap, Ap); tCp.stop(); tCp2 += tCp;
			pGen.start(); p = randB(pow(2, sizeP(Ap, epsilon)-1.0), pow(2, sizeP(Ap, epsilon))); 
			IP.nextprime(p,p); pGen.stop(); pGen2 += pGen;
			lamGen.start(); lambda = hashLambdaMod(C_Ap, Ap, p); lamGen.stop(); lamGen2 += lamGen;
			Field F(p);
			ZTZpZ.start(); DenseMatrix<Field> Am(A, F); //Am = A mod p
			//disp(Am);
			ZTZpZ.stop(); ZTZpZ2 += ZTZpZ;
			PLUQT.start();
			for(int i=0; i<(int)Am.rowdim(); i++){
				F.subin(Am.refEntry(i,i),lambda); // Am = Am-lambda*I
			}
			//disp(Am);
			P = FFLAS::fflas_new<size_t>(size);
			Q = FFLAS::fflas_new<size_t>(size);
			FFPACK::PLUQ(F, FFLAS::FflasNonUnit, size, size, Am.getPointer(), size, P, Q); //Am = LU
			PLUQT.stop(); PLUQT2 += PLUQT;
			
			deltGen.start(); Field::Element delta = delt(F, Am); deltGen.stop(); deltGen2 += deltGen; 
			Field::Element res = evalCA(F, C_Ap, lambda, mul, size); 
			/*
			bool RD = descartesRule(C_Ap, size);
			if(RD){cout << "Eigenvalues problem !" << endl;}
			else {cout << "OK" << endl;}
			*/
			writeCertificate(size, C_Ap, delta, lambda, mul, p, A, P, Am, Q);
		}
		else {
			/*
			if((double)A.getEntry(0,0)>=0.0){cout << "OK" << endl;}
			else {cout << "Eigenvalues problem !" << endl;}
			*/
			ofstream fichier("Certificate", ios::out | ios::app);
			fichier << size << endl;
			fichier << A.getEntry(0,0) << endl;
			fichier.close();
		}
	}
	cout << "Certificates successfully generated !" << endl;
	tim.stop();
	
	cout << endl << "Read file time : 		\t" << rfT << endl;
	cout << "Variables declaration time : 	\t" << vDT << endl;
	cout << "double to Rational conversion time : \t" << dTQ2 << endl;
	cout << "Rational to Integer conversion time : \t" << qTZ2 << endl;
	cout << "Charpoly time : 		\t" << tCp2 << endl;
	cout << "p generation time : 		\t" << pGen2 << endl;
	cout << "lambda generation time : 	\t" << lamGen2 << endl;
	cout << "Integer to Modular conversion time : \t" << ZTZpZ2 << endl;
	cout << "PLUQ decomposition time : 	\t" << PLUQT2 << endl;
	cout << "delta generation time : 	\t" << deltGen2 << endl;
	cout << "					---------------------------" << endl;
	cout << "				  	= " << (rfT+vDT+dTQ2+qTZ2+tCp2+pGen2+lamGen2+ZTZpZ2+deltGen2+PLUQT2) << endl;
	cout << "Total time : " << tim << endl << endl;
	return 0;
}

